DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `employeeId` varchar(5) NOT NULL,
  `email` varchar(20) DEFAULT NULL,
  `fullName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`employeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=lati

DROP TABLE IF EXISTS `audittrail`;
CREATE TABLE `audittrail` (
  `auditTrailId` varchar(5) NOT NULL,
  `accessBy` varchar(100) DEFAULT NULL,
  `accessDate` datetime DEFAULT NULL,
  PRIMARY KEY (`auditTrailId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1