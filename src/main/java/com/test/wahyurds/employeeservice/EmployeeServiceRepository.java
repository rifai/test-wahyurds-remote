package com.test.wahyurds.employeeservice;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeServiceRepository extends JpaRepository<EmployeeServiceDto, String>{

}
