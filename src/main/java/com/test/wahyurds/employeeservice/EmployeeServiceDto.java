package com.test.wahyurds.employeeservice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class EmployeeServiceDto {
	
	@Id
	@Column(name="employeeid")
	private String employeeId;
	
	@Column(name="email")
	private String email;
	
	@Column(name="fullname")
	private String fullName;
	
	public EmployeeServiceDto() {
		
	}
	
	public EmployeeServiceDto(String employeeId, String email, String fullName) {
		super();
		this.employeeId = employeeId;
		this.email = email;
		this.fullName = fullName;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Override
	public String toString() {
		return "EmployeeServiceDto [employeeId=" + employeeId + ", email=" + email + ", fullName=" + fullName + "]";
	}
}
