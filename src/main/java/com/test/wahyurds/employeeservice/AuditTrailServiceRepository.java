package com.test.wahyurds.employeeservice;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AuditTrailServiceRepository extends JpaRepository<AuditTrailServiceDto, String>{

}
