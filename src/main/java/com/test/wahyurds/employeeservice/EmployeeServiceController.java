package com.test.wahyurds.employeeservice;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeServiceController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private EmployeeServiceRepository employeeServiceRepository;
	
	@PostMapping("/employee-service")
	public EmployeeServiceDto addEmployee(@RequestBody EmployeeServiceDto productServiceDto) {
		EmployeeServiceDto employeeServiceDtoRes = employeeServiceRepository.save(productServiceDto);
		logger.info("{}", employeeServiceDtoRes);
		return employeeServiceDtoRes;
	}
	
	@PutMapping("/employee-service/id/{id}")
	public ResponseEntity<Object> updateEmployee(@RequestBody EmployeeServiceDto employeeServiceDtoReq,
			@PathVariable String id) {
		Optional<EmployeeServiceDto> employeeServiceDtoOptional = employeeServiceRepository.findById(id);
		if (!employeeServiceDtoOptional.isPresent())
			return ResponseEntity.notFound().build();

		employeeServiceDtoReq.setEmployeeId(id);
		employeeServiceRepository.save(employeeServiceDtoReq);
		logger.info("{}", employeeServiceDtoReq);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/employee-service/id/{id}")
	public void deleteById(@PathVariable String id) {
		employeeServiceRepository.deleteById(id);
	}
	
	@GetMapping("/employee-service/id/{id}")
	public EmployeeServiceDto findById(@PathVariable String id) {
		EmployeeServiceDto employeeServiceDto = employeeServiceRepository.findById(id).get();
		logger.info("{}", employeeServiceDto);
		return employeeServiceDto;
	}
	
}
