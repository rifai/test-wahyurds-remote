package com.test.wahyurds.employeeservice;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "audittrail")
public class AuditTrailServiceDto {
	
	@Id
	@Column(name="audittrailid")
	private String auditTrailId;
	
	@Column(name="accessby")
	private String accessBy;
	
	@Column(name="accessdate")
	private Date accessDate;
	
	public AuditTrailServiceDto() {
		
	}
	
	public AuditTrailServiceDto(String auditTrailId, String accessBy, Date accessDate) {
		super();
		this.auditTrailId = auditTrailId;
		this.accessBy = accessBy;
		this.accessDate = accessDate;
	}

	public String getAuditTrailId() {
		return auditTrailId;
	}

	public void setAuditTrailId(String auditTrailId) {
		this.auditTrailId = auditTrailId;
	}

	public String getAccessBy() {
		return accessBy;
	}

	public void setAccessBy(String accessBy) {
		this.accessBy = accessBy;
	}

	public Date getAccessDate() {
		return accessDate;
	}

	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}

	@Override
	public String toString() {
		return "EmployeeServiceDto [auditTrailId=" + auditTrailId + ", accessBy=" + accessBy + ", accessDate="
				+ accessDate + "]";
	}
	
}
